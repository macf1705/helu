import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import { ICountriesData, ISummaryData } from '../app.component';
import { DataManipulateService } from '../config/data-manipulate.service';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss'],
})
export class BarChartComponent implements OnChanges {
  constructor(private dataManipulateService: DataManipulateService) {}

  @Input() countryName: string;
  @Input() countriesData: ICountriesData[];
  @Input() summaryData: ISummaryData;

  public countries: ICountriesData[];
  public selectedCountry: ICountriesData[];
  public summary: ISummaryData;
  public barChartOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      yAxes: [
        {
          ticks: {
            stepSize: 1000000,
            beginAtZero: true,
          },
        },
      ],
    },
  };
  public barChartLabels: Label[] = ['Total confirmed'];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [];
  public barChartTotalConfirmedLabels: Label[] = ['Total confirmed'];
  public barChartTotalConfirmedData: ChartDataSets[];
  public barChartTotalDeathsLabels: Label[] = ['Total deaths'];
  public barChartTotalDeathsData: ChartDataSets[];
  public barChartTotalRecoveredLabels: Label[] = ['Total recovered'];
  public barChartTotalRecoveredData: ChartDataSets[];
  public barChartSelectedCountryLabels: Label[] = ['Total confirmed', 'Total deaths', 'Total recovered'];
  public barChartSelectedCountryData: ChartDataSets[];

  private setData(whatToReturn, country): [] {
    let data;
    switch (whatToReturn) {
      case 'totalConfirmed':
        data = [country.totalConfirmed];
        break;
      case 'totalDeaths':
        data = [country.totalDeaths];
        break;
      case 'totalRecovered':
        data = [country.totalRecovered];
        break;
      case 'all':
        data = [
          country.totalConfirmed,
          country.totalDeaths,
          country.totalRecovered
        ];
    }
    return data;
  }

  private createDataForBarChart(
    data: ICountriesData[],
    whatToReturn: 'totalConfirmed' | 'totalDeaths' | 'totalRecovered' | 'all'
  ): ChartDataSets[] {
    return data.map((country) => {
      const chartData = {
        data: this.setData(whatToReturn, country),
        label: country.countryName,
      };
      return chartData;
    });
  }

  private setDataForCharts(): void {
    const totalConfirmed = this.dataManipulateService.topCountries(
      this.countries,
      'totalConfirmed',
      10
    );
    const totalDeaths = this.dataManipulateService.topCountries(
      this.countries,
      'totalDeaths',
      10
    );
    const totalRecovered = this.dataManipulateService.topCountries(
      this.countries,
      'totalRecovered',
      10
    );

    this.barChartTotalConfirmedData = this.createDataForBarChart(
      totalConfirmed,
      'totalConfirmed'
    );
    this.barChartTotalDeathsData = this.createDataForBarChart(
      totalDeaths,
      'totalDeaths'
    );
    this.barChartTotalRecoveredData = this.createDataForBarChart(
      totalRecovered,
      'totalRecovered'
    );

    if (this.selectedCountry) {
      const globalData = {
        data: [
          this.summary.totalConfirmed,
          this.summary.totalDeaths,
          this.summary.totalRecovered,
        ],
        label: 'Global',
      };
      this.barChartSelectedCountryData = this.createDataForBarChart(
        this.selectedCountry,
        'all'
      );
      this.barChartSelectedCountryData.push(globalData);
    }
  }

  public ngOnChanges(changes: SimpleChanges): void {
    const country = changes.countryName;
    const countries = changes.countriesData;
    const summary = changes.summaryData;

    if (countries && countries.currentValue) {
      this.countries = this.countriesData;
    }

    if (
      country &&
      country.currentValue &&
      country.currentValue !== country.previousValue
    ) {
      this.selectedCountry = this.dataManipulateService.filterCountries(
        this.countries,
        country.currentValue.countryCode
      );
    } else {
      this.selectedCountry = null;
    }

    if (summary && summary.currentValue) {
      this.summary = this.summaryData;
    }

    this.setDataForCharts();
  }
}
