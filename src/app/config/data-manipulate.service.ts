import { Injectable } from '@angular/core';
import { ICountriesData } from '../results/results.component';
import _ from 'lodash';

@Injectable({ providedIn: 'root' })
export class DataManipulateService {
  public orderCountries(data: ICountriesData[]): ICountriesData[] {
    return _.orderBy(data, ['country'], ['asc']);
  }

  public filterCountries(
    countries: ICountriesData[],
    code: string
  ): ICountriesData[] {
    return _.filter(countries, {
      countryCode: code,
    });
  }

  public topCountries(
    data: ICountriesData[],
    field: string,
    amountOfResults: number
  ): ICountriesData[] {
    return _.slice(_.orderBy(data, [field], ['desc']), 0, amountOfResults);
  }
}
