import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class ConfigService {
  constructor(private http: HttpClient) {}

  apiUrl = 'https://api.covid19api.com/';

  public getAllCountries(): Observable<object> {
    return this.http
      .get<ICountry[]>(this.apiUrl + 'countries')
      .pipe(retry(3), catchError(this.handleError));
  }

  public getSummary(): Observable<object> {
    return this.http
      .get<ISummary>(this.apiUrl + 'summary')
      .pipe(retry(3), catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse): Observable<never> {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }

    return throwError('Something bad happened; please try again later.');
  }
}

export interface ISummaryGlobal {
  NewConfirmed: number;
  TotalConfirmed: number;
  NewDeaths: number;
  TotalDeaths: number;
  NewRecovered: number;
  TotalRecovered: number;
}

export interface ISummaryCountry {
  Country: string;
  CountryCode: string;
  Slug: string;
  NewConfirmed: number;
  TotalConfirmed: number;
  NewDeaths: number;
  TotalDeaths: number;
  NewRecovered: number;
  TotalRecovered: number;
  Date: string;
  Premium: any;
}

export interface ISummary {
  Message?: string;
  Global: ISummaryGlobal;
  Countries: ISummaryCountry[];
}

export interface ICountry {
  Country: string;
  Slug: string;
  ISO2: string;
}
