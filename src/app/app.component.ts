import { Component, OnInit } from '@angular/core';
import {
  ConfigService,
  ISummary,
  ISummaryCountry,
  ISummaryGlobal,
} from './config/config.service';

export interface ISummaryData {
  totalConfirmed: number;
  totalDeaths: number;
  totalRecovered: number;
}

export interface ICountriesData {
  countryName: string;
  countryCode: string;
  totalConfirmed: number;
  totalDeaths: number;
  totalRecovered: number;
}

export interface ICountriesNameData {
  countryName: string;
  countryCode: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  constructor(private configService: ConfigService) {}

  public summaryData: ISummaryData;
  public countriesData: ICountriesData[];
  public countriesNameData: ICountriesNameData[];
  public selectedCountry: ICountriesNameData;
  public selectedView = 'table';

  public ngOnInit(): void {
    this.showSummary();
  }

  public setCountry(country: ICountriesNameData): void {
    this.selectedCountry = country;
  }

  public setView(view: string): void {
    this.selectedView = view;
  }

  private createCountriesData(data: ISummaryCountry[]): ICountriesData[] {
    return data.map((item: ISummaryCountry) => {
      const newCountry = {
        countryName: item.Country,
        countryCode: item.CountryCode,
        totalConfirmed: item.TotalConfirmed,
        totalDeaths: item.TotalDeaths,
        totalRecovered: item.TotalRecovered,
      };
      return newCountry;
    });
  }

  private createCountryNameData(data: ISummaryCountry[]): ICountriesNameData[] {
    return data.map((item: ISummaryCountry) => {
      const newCountry = {
        countryName: item.Country,
        countryCode: item.CountryCode,
      };
      return newCountry;
    });
  }

  private createSummaryObject(data: ISummaryGlobal): ISummaryData {
    const summary = {
      totalConfirmed: data.TotalConfirmed,
      totalDeaths: data.TotalDeaths,
      totalRecovered: data.TotalRecovered,
    };
    return summary;
  }

  private showSummary(): void {
    this.configService.getSummary().subscribe((data: ISummary) => {
      (this.summaryData = this.createSummaryObject(data.Global)),
        (this.countriesData = this.createCountriesData(data.Countries)),
        (this.countriesNameData = this.createCountryNameData(data.Countries));
    });
  }
}
