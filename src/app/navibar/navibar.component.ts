import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import { ICountriesNameData } from '../app.component';
import _ from 'lodash';

@Component({
  selector: 'app-navibar',
  templateUrl: './navibar.component.html',
  styleUrls: ['./navibar.component.scss'],
})
export class NavibarComponent implements OnChanges {
  @Input() countriesNameData: ICountriesNameData[];
  @Output() selectedCountryEvent = new EventEmitter<ICountriesNameData>();
  @Output() currentViewEvent = new EventEmitter<string>();

  autocompleteSuggestions: ICountriesNameData[];
  autocompletePlaceholder = 'Search country';
  autocompleteShow = true;
  countryName = '';
  currentView = 'table';

  constructor() {}

  public ngOnChanges(changes: SimpleChanges): void {}

  private autocompleteShowSuggestions(query: string): void {
    this.autocompleteSuggestions = _.filter(
      this.countriesNameData,
      (country: ICountriesNameData) => {
        return (
          country.countryName.toLowerCase().indexOf(query.toLowerCase()) >= 0
        );
      }
    );

    if (this.autocompleteSuggestions.length > 10) {
      this.autocompleteSuggestions = this.autocompleteSuggestions.slice(0, 10);
    }

    this.autocompleteShow = true;
  }

  public autocomplete(event: any): void {
    const query = event.target.value;

    if (query.length > 1) {
      this.autocompleteShowSuggestions(query);
    } else {
      this.autocompleteSuggestions = null;
      this.selectedCountryEvent.emit(null);
    }
  }

  public chooseCountry(country: ICountriesNameData): void {
    this.selectedCountryEvent.emit(country);
    this.autocompleteShow = false;
  }

  public switchView(viewMode: string): void {
    if (this.currentView === viewMode) {
      return;
    }
    this.currentViewEvent.emit(viewMode);
    this.currentView = viewMode;
  }
}
