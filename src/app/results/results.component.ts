import { Component, OnChanges, SimpleChanges, Input } from '@angular/core';
import { DataManipulateService } from '../config/data-manipulate.service';
import _ from 'lodash';

export interface ISummaryData {
  totalConfirmed: number;
  totalDeaths: number;
  totalRecovered: number;
}

export interface ICountriesData {
  countryName: string;
  countryCode: string;
  totalConfirmed: number;
  totalDeaths: number;
  totalRecovered: number;
}

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss'],
})
export class ResultsComponent implements OnChanges {
  constructor(private dataManipulateService: DataManipulateService) {}
  @Input() countryName: string;
  @Input() countriesData: ICountriesData[];
  @Input() summaryData: ISummaryData;

  public countries: ICountriesData[];
  public filteredCountries: ICountriesData[];
  public summary: ISummaryData;

  public ngOnChanges(changes: SimpleChanges): void {
    const country = changes.countryName;
    const countries = changes.countriesData;
    const summary = changes.summaryData;

    if (countries && countries.currentValue) {
      this.countries = this.filteredCountries = this.dataManipulateService.orderCountries(
        this.countriesData
      );
    }

    if (
      country &&
      country.currentValue &&
      country.currentValue !== country.previousValue
    ) {
      this.filteredCountries = this.dataManipulateService.filterCountries(
        this.countries,
        country.currentValue.countryCode
      );
    }

    if (country && !country.currentValue) {
      this.filteredCountries = this.countries;
    }

    if (summary && summary.currentValue) {
      this.summary = this.summaryData;
    }
  }
}
